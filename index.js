const express = require("express");
const bodyParser = require("body-parser");
const routes = require("./app/routes/routes")

const app = express();
const port = process.env.PORT || 80;
console.log("v 1.8 starting");
app.listen(port);
app.use(bodyParser.urlencoded({
    extended: true
 }));
app.use(bodyParser.json());
app.use(routes);

console.log("API started");