
const databaseConnection = require('../config/DBConnection');

exports.getMedewerkers = function(req, res){
    console.log("Getting all medewerkers!");
    try{
        databaseConnection.query("SELECT * FROM medewerkers", function (err, rows, fields){
            if(err){
                res.status(500).send(err);
            }
            console.log(rows);
            console.log(fields);
           res.status(200).send(rows);
        });
        return;
    }  
    catch(e){
        console.log(e);
        res.status(500).send(e);
    }
}

exports.getDienstverbanden = function(req, res){
    console.log("Getting all Dienstverbanden!");
    try{
        databaseConnection.query("SELECT * FROM dienstverband", function (err, rows, fields){
            if(err){
                res.status(500).send(err);
            }
            console.log(rows);
            console.log(fields);
           res.status(200).send(rows);
        });
        return;
    }  
    catch(e){
        console.log(e);
        res.status(500).send(e);
    }
}

exports.getLooncomponenten = function(req, res){
    console.log("Getting all looncomponenten!");
    try{
        databaseConnection.query("SELECT * FROM looncomponenten", function (err, rows, fields){
            if(err){
                res.status(500).send(err);
            }
            console.log(rows);
            console.log(fields);
           res.status(200).send(rows);
        });
        return;
    }  
    catch(e){
        console.log(e);
        res.status(500).send(e);
    }
}

exports.getStandenregisters = function(req, res){
    console.log("Getting all standenregisters!");
    try{
        databaseConnection.query("SELECT * FROM standenregister", function (err, rows, fields){
            if(err){
                res.status(500).send(err);
            }
            console.log(rows);
            console.log(fields);
           res.status(200).send(rows);
        });
        return;
    }  
    catch(e){
        console.log(e);
        res.status(500).send(e);
    }
}

exports.reset = async function(req, res){
    try{
        await databaseConnection.query("SET foreign_key_checks = 0");
        await databaseConnection.query("TRUNCATE TABLE medewerkers");
        await databaseConnection.query("TRUNCATE TABLE dienstverband");
        await databaseConnection.query("TRUNCATE TABLE looncomponenten");
        await databaseConnection.query("TRUNCATE TABLE standenregister");
        await databaseConnection.query("TRUNCATE TABLE standenregister2");
        await databaseConnection.query("SET foreign_key_checks = 1");
        res.status(200).send("Database reset!");
    }
    catch(e){
        console.log(e);
        res.status(500).send(e);
    }
}