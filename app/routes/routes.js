var express = require("express");
var router = express.Router();
var payrollRepository = require("../repository/payrollRepository");

router.get("/running", (req, res) =>{
  console.log("got running command");
  res.status(200).send({ message: "The app is running!" })
  }  
);

router.get("/medewerkers", 
    payrollRepository.getMedewerkers
);

router.get("/dienstverbanden", 
    payrollRepository.getDienstverbanden
);

router.get("/standenregisters", 
    payrollRepository.getStandenregisters
);

router.get("/looncomponenten", 
    payrollRepository.getLooncomponenten
);

router.get("/reset", payrollRepository.reset
);

router.get("/break", (req, res) => {
  process.exit(1);
})

router.all("*", (req, res) =>{
  console.log("got something else");
  res.status(405).send({ message: "Wrong stuff" })
  }  );


module.exports = router;